package cbm.itp.vegmpro;

import android.app.Activity;
import android.os.Bundle;

import cbm.itp.vegmpro.utils.Utility;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class ZXingActivity extends Activity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZBarScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }


    @Override
    public void handleResult(me.dm7.barcodescanner.zbar.Result rawResult) {
        // Do something with the result here
        Utility.showMessage(this, rawResult.getContents());
//        Utility.showMessage(this, rawResult.getBarcodeFormat().toString());

        // If you would like to resume scanning, call this method below:
//        mScannerView.resumeCameraPreview(this);
    }
}